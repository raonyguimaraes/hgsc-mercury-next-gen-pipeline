#!/usr/bin/ruby


$:.unshift File.join(File.dirname(__FILE__), ".", "..", "lib")

require 'BWAParams'
require 'PipelineHelper'
require 'yaml'
require 'PathInfo'
require '/hgsc_software/Valence/Valence_13.1/lib/ProtocolObjects'

class DNASeq_BWA_Aligner
  # Constructor - prepare the context
  def initialize()
    begin
      readConfigParams()
      findSequenceFiles()
    rescue Exception => e
      $stderr.puts e.message
      $stderr.puts e.backtrace.inspect
      exit -1
    end
  end

  # Create cluster jobs to start the alignment
  def process()
    puts "\nCreating XML DNASeq_BWA_AlignerCommands.xml using API\n"
    @alignerCommands = Procedure.new()
    @alignerCommands.stepName = 'DNASeq_BWA_AlignerCommands'

    @alignerCommands.addSetGlobal(Variable.new("reference",@reference))
    @alignerCommands.addSetGlobal(Variable.new("inputBam",@finalBamName))
    @alignerCommands.addSetGlobal(Variable.new("fcBarcode",@fcBarcode))

    if @reference.downcase.eql?("sequence") || @reference.eql?("N/A") || @reference.eql?("N/A_auto")
      puts "No alignment to perform since provided reference from LIMS is: " + @reference.to_s
      puts "Running postrun script to upload available stats (Only FASTQ stats, instrument run stats) and finish off pipeline analysis."
      runPostRunCmd()
      exit 0
    end
    puts "FOR ALIGNMENT USING GENOME REFERENCE IN PATH : " + @reference.to_s

    if @zippedSequences == true
      protocolUnzipSequence = Procedure.fromXMLFile(PathInfo::XML_PROTOCOLS_DIR + "/Production/UnzipSequenceFiles.xml")

      # Save the suffix .bz2 from the sequence file names to distinguish between 
      # zipped and unzipped FASTQ files
      idx = 0
      @sequenceFilesZipped = []
      while idx < @sequenceFiles.length
        @sequenceFilesZipped[idx] = @sequenceFiles[idx].dup
        @sequenceFiles[idx].gsub!(/\.bz2$/, "")
        idx = idx + 1
      end

      if @isFragment == true    #Run is single-end. Accept only 1 zipped FASTQ to unzip. Remove an action.
        protocolUnzipSequence.removeStep("${sampleID}_RunUnzipCommandFile2")
        protocolUnzipSequence.addSetGlobal(Variable.new("inputZippedFile1",@sequenceFilesZipped[0]))
      else                      #Run is paried-end. Unzip 2 FASTQ files.
        protocolUnzipSequence.addSetGlobal(Variable.new("inputZippedFile1",@sequenceFilesZipped[0]))
        protocolUnzipSequence.addSetGlobal(Variable.new("inputZippedFile2",@sequenceFilesZipped[1]))
      end
      @alignerCommands.appendStep(protocolUnzipSequence)
    end

    # Run BWA aln command(s)

    protocolBWACommands = Procedure.fromXMLFile(PathInfo::XML_PROTOCOLS_DIR + "/Production/BWA_aln1_aln2_sampe_samse.xml")

    if @isFragment == false    #Run is paired-end. Remove bwa samse From XML and provide FASTQ2
      protocolBWACommands.removeStep("${sampleID}_bwaSamse")
      protocolBWACommands.addSetGlobal(Variable.new("FASTQfile2",@sequenceFiles[1]))
    else                       #Run is single-end. Remove aln READ2 and bwa sampe steps
      protocolBWACommands.removeStep("${sampleID}_bwaSampe")
      protocolBWACommands.removeStep("${sampleID}_bwaAln2")
    end

    protocolBWACommands.addSetGlobal(Variable.new("FASTQfile1",@sequenceFiles[0]))
    protocolBWACommands.addSetGlobal(Variable.new("RGstring",buildRGString()))
    protocolBWACommands.addSetGlobal(Variable.new("samFile",@samFileName.to_s))
    @alignerCommands.appendStep(protocolBWACommands)
    if @zippedSequences == true
      protocolBWACommands.dependencies.add(protocolUnzipSequence.stepName)
    end


    # At this stage, BWA would have finished generating a SAM file. Process it
    # to make a bam. After BAM is ready, run BAM Analyzer on it.
    # This step converts SAM to BAM, sorts it, mark duplicates, fixes
    # mate reads and generates alignment statistics (BAM Analyzer).

    @protocolSAMtoBAM=Procedure.fromXMLFile(PathInfo::XML_PROTOCOLS_DIR + "/Production/ProcessSAMtoBAMDriver.xml")
    @alignerCommands.appendStep(@protocolSAMtoBAM)
    @protocolSAMtoBAM.dependencies.add(protocolBWACommands.stepName)
    @lastCommandinProtocol = @protocolSAMtoBAM
    @protocolSAMtoBAM.addSetGlobal(Variable.new("isFragment",@isFragment.to_s))
    @protocolSAMtoBAM.addSetGlobal(Variable.new("outputBAM",@finalBamName))

    # If a capture chip design file was specified, run capture stats.
    # If the name of the chip design ended with "none", ignore running capture
    # stats.
    if @chipDesign != nil && !@chipDesign.empty?() &&
        !@chipDesign.downcase.match(/none$/)
      protocolCaptureStats=Procedure.fromXMLFile(PathInfo::XML_PROTOCOLS_DIR + "/Production/CaptureStatsDriver.xml")
      @alignerCommands.appendStep(protocolCaptureStats)
      protocolCaptureStats.dependencies.add(@protocolSAMtoBAM.stepName)
      protocolCaptureStats.addSetGlobal(Variable.new("captureDesign",@chipDesign))
      @lastCommandinProtocol = protocolCaptureStats
    end

    if true == isHumanSample19()   #Run GATK, SNP/INDEL Calling, Cassandra, MendelianOutput, VCFanalyzer for hg19 samples
      protocolGATK_SNP_INDEL_calling=Procedure.fromXMLFile(PathInfo::XML_PROTOCOLS_DIR + "/Mercury/Mercury.xml")
      # reference setGlobal already set previously. 
      # inputBam setGlobal already set previously.
      protocolGATK_SNP_INDEL_calling.addSetGlobal(Variable.new("sampleName",@sampleName))
      protocolGATK_SNP_INDEL_calling.addSetGlobal(Variable.new("maxPileupReads",1024))
      @alignerCommands.appendStep(protocolGATK_SNP_INDEL_calling)
      protocolGATK_SNP_INDEL_calling.dependencies.add(@lastCommandinProtocol.stepName)
      @lastCommandinProtocol = protocolGATK_SNP_INDEL_calling
    end

    if true == isMitoSample()
      protocolGATK_SNP_INDEL_calling=Procedure.fromXMLFile(PathInfo::XML_PROTOCOLS_DIR + "/Mercury/Mercury.xml")
      # reference setGlobal already set previously. 
      # inputBam setGlobal already set previously.
      protocolGATK_SNP_INDEL_calling.addSetGlobal(Variable.new("sampleName",@sampleName))
      protocolGATK_SNP_INDEL_calling.addSetGlobal(Variable.new("maxPileupReads",100000000000))
      @alignerCommands.appendStep(protocolGATK_SNP_INDEL_calling)
      protocolGATK_SNP_INDEL_calling.dependencies.add(@lastCommandinProtocol.stepName)
      protocolCreateBEDGraph=Procedure.fromXMLFile(PathInfo::XML_PROTOCOLS_DIR + "/Production/CreateBEDGraph.xml")
      @alignerCommands.appendStep(protocolCreateBEDGraph)
      protocolCreateBEDGraph.dependencies.add(protocolGATK_SNP_INDEL_calling.stepName)
      @lastCommandinProtocol = protocolCreateBEDGraph
    end


    # Now hook up the post run command here
    runPostRunCmd()
  end

  private

  # Read the configuration file containing input parameters
  def readConfigParams()
    inputParams = BWAParams.new()
    inputParams.loadFromFile()
    @reference      = inputParams.getReferencePath()  # Reference path
    @filterPhix     = inputParams.filterPhix?()       # Whether to filter phix reads
    @libraryName    = inputParams.getLibraryName()    # Obtain library name
    @chipDesign     = inputParams.getChipDesignName() # Chip design name for capture
                                                      # stats calculation
    @sampleName     = inputParams.getSampleName()     # Sample name
    @rgPUField      = inputParams.getRGPUField()      # PU field for RG tag

    @fcBarcode      = inputParams.getFCBarcode()       # Lane barcode FCName-Lane-BarcodeName
    @baseQualFormat = inputParams.getBaseQualFormat()  # Sanger or Illumina format

    # Validate the parameters
    if @reference == nil || @reference.empty?()
      raise "ERROR: Reference path must be specified in config file"
    elsif @fcBarcode == nil || @fcBarcode.empty?()
      raise "ERROR: Flowcell barcode must be specified in config file using key FC_BARCODE"
    end

    # Create file names for subsequent stages
    @samFileName  = @fcBarcode + ".sam"
    @finalBamName = @fcBarcode + "_marked.bam"

    puts "SAM file name will be: " + @samFileName
    puts "BAM file name will be: " + @finalBamName
  end

  # Find the sequence files and determine if the sequence event is fragment or
  # paired-end
  def findSequenceFiles()
    fileList = nil

    puts "Searching for FASTQ sequence files"
    fileList = PipelineHelper.findSequenceFiles(Dir.pwd)

    if fileList == nil || fileList.size < 1
      raise "ERROR: Could not find correct number of required FASTQ sequence files in directory " + Dir.pwd
    elsif fileList.size == 1
      @isFragment = true
      puts "Processing reads as single-end"
    elsif fileList.size == 2
      @isFragment = false
      puts "Processing reads as paired-end."
    elsif fileList.size > 2
      raise "ERROR: More than two FASTQ sequence files found in directory " + Dir.pwd
    end

    # The fileList is already sorted by PipelineHelper (forward read is index 0, reverse read is index 1)
    @sequenceFiles = fileList

    puts "Found sequence files "
    @sequenceFiles.each do |seqFile|
      if seqFile.match(/\.bz2$/)
        @zippedSequences = true
      end
      puts seqFile.to_s
    end
  end

  # Returns the value string for RG tag
  def buildRGString()
    currentTime = Time.new
    rgString = "@RG\\tID:0\\tSM:"

    if @sampleName != nil && !@sampleName.empty?()
      rgString = rgString + @sampleName.to_s
    else
      rgString = rgString + @fcBarcode.to_s
    end

    if @libraryName != nil && !@libraryName.empty?()
      rgString = rgString + "\\tLB:" + @libraryName.to_s
    end

    # If PU field was already obtained from config params, use that. Use a
    # dummy PU field (fcbarcode) if it was not already available.
    if @rgPUField != nil && !@rgPUField.empty?()
      rgString = rgString + "\\tPU:" + @rgPUField.to_s
    else
      rgString = rgString + "\\tPU:" + @fcBarcode.to_s
    end

    rgString = rgString + "\\tCN:BCM\\tDT:" + currentTime.strftime("%Y-%m-%dT%H:%M:%S%z")
    rgString = rgString + "\\tPL:Illumina"
    #    rgString = rgString + "'"
    return rgString.to_s
  end

  # Read the reference path and determine if the given sequencing event is human
  # or not. More specifically, if will be mapped to hg19 human.
  def isHumanSample19()
    if @reference != nil && @reference.match(/hg19\.fa$/)
      return true
    else
      return false
    end
  end

  # Read the reference path and determine if the given sequencing event is a mitochondrial sample
  def isMitoSample()
    if @reference != nil && @reference.match(/NC\_012920\.1\-mitochondrial-genome\.fasta$/)
      return true
    else
      return false
    end
  end

  # Command to run after alignment completes. Removes intermediate files and emails and uploads stats
  def runPostRunCmd()
    protocolPostAlignmentProcess = Procedure.fromXMLFile(PathInfo::XML_PROTOCOLS_DIR + "/Production/PostAlignmentProcessDriver.xml")
    @alignerCommands.appendStep(protocolPostAlignmentProcess)

    # Add dependency.. it could be the BWA Aligner, or the CaptureStats, or NONE 
    if @lastCommandinProtocol !=nil #&& !@lastCommandinProtocol.empty?()
      protocolPostAlignmentProcess.dependencies.add(@lastCommandinProtocol.stepName)
    end

    @alignerCommands.toXML().write(targetstr="", 8)
    puts "\Finished creating DNASeq_BWA_AlignerCommands.xml. Writing and closing file.\n"
    fileHandle = File.new("DNASeq_BWA_AlignerCommands.xml", "w")
    fileHandle.puts(targetstr)
    fileHandle.close()
    puts "\nExecuting DNASeq_BWA_AlignerCommands.xml through Valence\n"
    `rm DNASeq_BWA_AlignerCommandsSubstituted.xml` if File.exist?("DNASeq_BWA_AlignerCommandsSubstituted.xml")
    executeValenceCmd = "Valence.py DNASeq_BWA_AlignerCommands.xml --output=DNASeq_BWA_AlignerCommandsSubstituted.xml --globalsOut=DNASeq_BWA_AlignerGlobals.json"
    output = `#{executeValenceCmd}`
    puts output

  end
end

obj = DNASeq_BWA_Aligner.new()
obj.process()
